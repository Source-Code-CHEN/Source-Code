package com.tec.compment.config;

import com.tec.compment.business.dao.HelloDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description AutoWiredConfig
 * @Date 2020/5/6 15:50
 */
@Configuration
@ComponentScan(value ={"com.tec.compment.business", "com.tec.compment.entity"})
public class AutoWiredConfig {


	@Bean("helloDao2")
	//@Primary
	public HelloDao helloDao(){
		return new HelloDao().setFlag("2");
	}


}
