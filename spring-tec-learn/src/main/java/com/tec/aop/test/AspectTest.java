package com.tec.aop.test;

import com.tec.aop.business.Calculator;
import com.tec.aop.config.AopConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description Test
 * @Date 2020/5/12 9:48
 */
public class AspectTest {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext(AopConfig.class);

		Calculator calculator = ac.getBean(Calculator.class);

		calculator.div(1,0);
	}
}
