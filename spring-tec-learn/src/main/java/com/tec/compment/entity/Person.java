package com.tec.compment.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description Person
 * @Date 2020/5/6 14:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Person {

    private String name;

    private Integer age;

}
