package com.tec.compment.entity;

import lombok.*;


/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description Dog
 * @Date 2020/5/8 10:28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Dog {

	private String name;

	private Integer age;

	private String color;
}
