package com.tec.compment.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description FactoryBeanConfig
 * @Date 2020/5/6 16:15
 */
@Configuration
@ComponentScan(value = "com.tec.compment.factorybean")
public class FactoryBeanConfig {


}
