package com.tec.compment.filter;

import org.springframework.core.io.Resource;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;

import java.io.IOException;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description TestTypeFilter
 * @Date 2020/5/6 15:59
 */
public class TestTypeFilter implements TypeFilter {

	@Override
    public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory)
            throws IOException {
        AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();

        ClassMetadata classMetadata = metadataReader.getClassMetadata();

        Resource resource = metadataReader.getResource();
		return true;
//        if(resource.getFilename().contains("Service")){
//            return true;
//        }
//        return false;
    }
}
