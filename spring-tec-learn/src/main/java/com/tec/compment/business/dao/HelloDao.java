package com.tec.compment.business.dao;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description HelloDao
 * @Date 2020/5/6 15:53
 */
@Component
@Data
@ToString
@Accessors(chain = true)
public class HelloDao {

	private String  flag = "1";

}
