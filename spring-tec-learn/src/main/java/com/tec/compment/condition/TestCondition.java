package com.tec.compment.condition;


import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description TestCondition
 * @Date 2020/5/7 9:35
 */
public class TestCondition implements Condition {

	@Override
    public boolean matches(ConditionContext conditionContext,
						   AnnotatedTypeMetadata annotatedTypeMetadata) {
        return true;
    }
}
