package com.tec.compment.factorybean;


import com.tec.compment.entity.Dog;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description DogFactoryBean
 * @Date 2020/5/8 10:29
 */
@Component
public class DogFactoryBean implements FactoryBean<Dog> {

    @Override
    public Dog getObject() throws Exception {
        return new Dog();
    }

    @Override
    public Class<?> getObjectType() {
        return Dog.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
