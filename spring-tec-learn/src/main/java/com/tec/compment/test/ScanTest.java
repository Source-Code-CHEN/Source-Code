package com.tec.compment.test;

import com.tec.compment.config.ScanConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description ScanTest
 * @Date 2020/5/12 15:01
 */
public class ScanTest {


    public static void main(String[] args) {

        ApplicationContext ac = new AnnotationConfigApplicationContext(ScanConfig.class);

        String[] beanNamesForType = ac.getBeanDefinitionNames();
        for (String beanName : beanNamesForType) {
            System.out.println(beanName);
        }

    }

}
