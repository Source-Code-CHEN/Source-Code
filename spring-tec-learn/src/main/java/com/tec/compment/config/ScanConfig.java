package com.tec.compment.config;

import com.tec.compment.filter.TestTypeFilter;
import com.tec.compment.imports.ImportBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description ScanConfig
 * @Date 2020/5/6 15:50
 */
@Configuration
@ComponentScan(value = "com.tec.compment.business",includeFilters = {
        @ComponentScan.Filter(type= FilterType.CUSTOM,classes ={TestTypeFilter.class})
},useDefaultFilters = false)
@Import({ImportBean.class})
public class ScanConfig {



}
