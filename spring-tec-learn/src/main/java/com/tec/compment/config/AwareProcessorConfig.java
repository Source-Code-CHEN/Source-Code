package com.tec.compment.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description AwareProcessorConfig
 * @Date 2020/5/11 16:25
 */
@Configuration
@ComponentScan(value ={"com.tec.compment.entity"})
public class AwareProcessorConfig {

}
