package com.tec.compment.test;

import com.tec.compment.config.AutoWiredConfig;
import com.tec.compment.entity.Moon;
import com.tec.compment.entity.Sun;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description AutoWiredTest
 * @Date 2020/5/9 16:07
 */
public class AutoWiredTest {


    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac  = new AnnotationConfigApplicationContext(AutoWiredConfig.class);
//        HelloService helloService = ac.getBean(HelloService.class);
//		helloService.print();
//		//根据类型去查找bean的时候
//        HelloDao helloDao = ac.getBean(HelloDao.class);
//        System.out.println(helloDao);


		  Sun sun = ac.getBean(Sun.class);
		  Moon moon = ac.getBean(Moon.class);
		  System.out.println(sun.getMoon());
		  System.out.println(moon);
	}
}
