package com.tec.compment.test;

import com.tec.compment.config.FactoryBeanConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description FactoryBeanTest
 * @Date 2020/5/9 17:35
 */
public class FactoryBeanTest {

	public static void main(String[] args) {
		ApplicationContext ac = new AnnotationConfigApplicationContext(FactoryBeanConfig.class);

		Object bean1 = ac.getBean("dogFactoryBean");
		System.out.println(bean1);


		Object bean2 = ac.getBean("&dogFactoryBean");
		System.out.println(bean2);
	}
}
