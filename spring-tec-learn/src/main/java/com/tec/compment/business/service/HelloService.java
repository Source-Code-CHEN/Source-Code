package com.tec.compment.business.service;

import com.tec.compment.business.dao.HelloDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description HelloService
 * @Date 2020/5/6 15:53
 */
@Service
public class HelloService {

	@Qualifier("helloDao")
    @Autowired
    private HelloDao helloDao;

    public void print(){
        System.out.println("from service : " + helloDao);
    }
}
