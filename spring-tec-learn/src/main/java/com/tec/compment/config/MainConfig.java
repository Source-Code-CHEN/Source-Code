package com.tec.compment.config;

import com.tec.compment.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description MainConfig
 * @Date 2020/5/6 14:30
 */
@Configuration
public class MainConfig {



    @Bean
    public Person person(){
        return new Person("xm",16);
    }

}
