package com.tec.compment.config;

import com.tec.compment.entity.Person;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description ScopeConfig
 * @Date 2020/5/6 16:15
 */
@Configuration
public class ScopeConfig {

    @Bean
    @Lazy
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public Person person(){

        System.out.println("创建一个对象");
        return new Person("xm",16);
    }
}
