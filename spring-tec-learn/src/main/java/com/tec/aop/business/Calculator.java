package com.tec.aop.business;

import org.springframework.stereotype.Component;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description Calculator
 * @Date 2020/5/12 10:29
 */
@Component
public class Calculator {

	public int div(int a , int b){
		int x = 0;
		try {
			x = a/b;
		}
		catch (Exception e){

		}
		return  x;
	}
}
