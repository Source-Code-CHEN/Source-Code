package com.tec.aop.log;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description logAspects
 * @Date 2020/5/12 10:30
 */
@Aspect
public class LogAspects {

	@Pointcut("execution(public int com.tec.aop.business.Calculator.*(..))")
	public void pointCut(){

	}

	@Before("pointCut()")
	public void logBefore(JoinPoint joinPoint){
		Object[] args = joinPoint.getArgs();
		List result = Arrays.stream(args).collect(Collectors.toList());
		System.out.println("方法Before执行前调用,参数列表{ "+result+" }");
	}

	@After("pointCut()")
	public void logAfter(){
		System.out.println("方法After执行后调用");
	}

	@AfterReturning(value = "pointCut()",returning = "result")
	public void logReturn(Object result)
	{
		System.out.println("方法Return返回调用,返回的结果是 { "+ result+" }");
	}


	@AfterThrowing(value = "pointCut()",throwing = "exception")
	public void logException(Exception exception){
		System.out.println("方法Exception报错后调用 ,报错信息 { "+exception+" }");
	}

//	@Around("pointCut()")
// 	public Object logAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
//		System.out.println("方法Around执行前调用,参数列表{}");
//		Object proceed = proceedingJoinPoint.proceed();
//		System.out.println("方法Around执行后调用,参数列表{}");
//		return proceed;
//	}

}
