package com.tec.compment.config;

import com.tec.compment.condition.TestCondition;
import com.tec.compment.entity.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description ConditionConfig
 * @Date 2020/5/6 16:15
 */
@Configuration
public class ConditionConfig {

    @Bean
    @Conditional(value = TestCondition.class)
    public Person person(){
        System.out.println("创建一个对象");
        return new Person("xm",16);
    }
}
