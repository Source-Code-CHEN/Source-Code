package com.tec.compment.entity;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description AwareProcessor
 * @Date 2020/5/11 16:22
 */
@Component
public class AwareProcessor implements BeanPostProcessor{

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println(beanName + " before : 进入postProcessBeforeInitialization");
		return bean;
	}


	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		System.out.println(beanName + " After: 进入postProcessAfterInitialization");
		return bean;
	}



}
