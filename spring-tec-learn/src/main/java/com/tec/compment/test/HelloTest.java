package com.tec.compment.test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description HelloTest
 * @Date 2020/5/9 17:29
 */
public class HelloTest {

	public static void main(String[] args) {

		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext();

		System.out.println("hello word");
	}
}
