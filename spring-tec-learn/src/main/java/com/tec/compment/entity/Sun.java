package com.tec.compment.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description Sun
 * @Date 2020/5/11 16:05
 */
@Component
public class Sun {

	@Autowired
	private Moon moon;

	//@Autowired
	public Sun(Moon moon) {
		this.moon = moon;
	}

	public Moon getMoon() {
		return moon;
	}

	//@Autowired
	public void setMoon(Moon moon) {
		this.moon = moon;
	}

	@PostConstruct
	public void init(){
		System.out.println("进入init方法");
	}
}
