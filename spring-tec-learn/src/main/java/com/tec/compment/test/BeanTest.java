package com.tec.compment.test;

import com.tec.compment.config.FactoryBeanConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description BeanTest
 * @Date 2020/5/6 14:36
 */
public class BeanTest {

    public static void main(String[] args) {

//        ApplicationContext ac = new AnnotationConfigApplicationContext(MainConfig.class);
//
//        Person bean = ac.getBean(Person.class);
//        System.out.println(bean);
//
//        String[] beanNamesForType = ac.getBeanNamesForType(Person.class);
//        for (String s : beanNamesForType) {
//            System.out.println(s);
//        }


//        ApplicationContext ac = new AnnotationConfigApplicationContext(ScanConfig.class);
//
//        String[] beanNamesForType = ac.getBeanDefinitionNames();
//        for (String beanName : beanNamesForType) {
//            System.out.println(beanName);
//        }

//        ApplicationContext ac = new AnnotationConfigApplicationContext(ScopeConfig.class);
//        String[] beanNamesForType = ac.getBeanDefinitionNames();
//        for (String beanName : beanNamesForType) {
//            System.out.println(beanName);
//        }
//        System.out.println("获取bean1");
//        Person bean1 = ac.getBean(Person.class);
//        System.out.println("获取bean2");
//        Person bean2 = ac.getBean(Person.class);
//
//        System.out.println(bean1 == bean2);

//        ApplicationContext ac = new AnnotationConfigApplicationContext(ConditionConfig.class);
//        String[] beanNamesForType = ac.getBeanDefinitionNames();
//        for (String beanName : beanNamesForType) {
//            System.out.println(beanName);
//        }


        ApplicationContext ac = new AnnotationConfigApplicationContext(FactoryBeanConfig.class);

//        String[] beanNamesForType = ac.getBeanDefinitionNames();
//        for (String beanName : beanNamesForType) {
//            System.out.println(beanName);
//        }
//        DogFactoryBean bean = ac.getBean(DogFactoryBean.class);
//        System.out.println(bean);


        Object bean1 = ac.getBean("dogFactoryBean");
        System.out.println(bean1);

    }

}
