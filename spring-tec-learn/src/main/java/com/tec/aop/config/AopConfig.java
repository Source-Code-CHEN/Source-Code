package com.tec.aop.config;

import com.tec.aop.business.Calculator;
import com.tec.aop.log.LogAspects;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author chenxm66777123
 * @version 1.0.0
 * @Description AopConfig
 * @Date 2020/5/12 9:49
 */
@Configuration
@EnableAspectJAutoProxy
public class AopConfig {

	@Bean
	public Calculator calculator(){
		return new Calculator();
	}


	@Bean
	public LogAspects logAspects(){
		return new LogAspects();
	}
}
